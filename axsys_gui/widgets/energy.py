from qtpy.QtWidgets import (
    QGroupBox,
    QVBoxLayout,
    QHBoxLayout,
    QComboBox,
    QPushButton,
    QMessageBox,
    QDialog,
)
from nbs_gui.widgets.views import AutoControl, AutoMonitor
from nbs_gui.widgets.motor import MotorMonitor, MotorControl
from nbs_gui.widgets.manipulator_monitor import (
    ManipulatorMonitor,
    PseudoManipulatorControl,
)


class UE52EnergyControl(QGroupBox):
    def __init__(self, energy, parent_model, *args, orientation=None, **kwargs):
        super().__init__("Energy Monitor", *args, **kwargs)
        hbox = QHBoxLayout()

        vbox = QVBoxLayout()
        vbox.addWidget(AutoControl(energy.monochromator.energy, parent_model))
        vbox.addWidget(AutoControl(energy.monochromator.slit, parent_model))
        vbox.addWidget(AutoControl(energy.monochromator.cff, parent_model))
        vbox.addWidget(AutoControl(energy.monochromator.harmonic, parent_model))
        vbox.addWidget(AutoControl(energy.monochromator.idon, parent_model))

        vbox2 = QVBoxLayout()
        vbox2.addWidget(AutoControl(energy.undulator.gap, parent_model))
        vbox2.addWidget(AutoControl(energy.undulator.shift, parent_model))
        vbox2.addWidget(AutoControl(energy.undulator.mode, parent_model))

        hbox.addLayout(vbox)
        hbox.addLayout(vbox2)

        self.setLayout(hbox)



class UE52EnergyMonitor(QGroupBox):
    def __init__(self, energy, parent_model, *args, orientation=None, **kwargs):
        super().__init__("Energy Monitor", *args, **kwargs)
        hbox = QHBoxLayout()

        vbox = QVBoxLayout()
        vbox.addWidget(AutoMonitor(energy.monochromator.energy, parent_model))
        vbox.addWidget(AutoMonitor(energy.monochromator.slit, parent_model))
        vbox.addWidget(AutoMonitor(energy.monochromator.cff, parent_model))
        vbox.addWidget(AutoMonitor(energy.monochromator.harmonic, parent_model))

        vbox2 = QVBoxLayout()
        vbox2.addWidget(AutoMonitor(energy.undulator.gap, parent_model))
        vbox2.addWidget(AutoMonitor(energy.undulator.shift, parent_model))
        vbox2.addWidget(AutoMonitor(energy.undulator.mode, parent_model))

        hbox.addLayout(vbox)
        hbox.addLayout(vbox2)

        self.setLayout(hbox)
