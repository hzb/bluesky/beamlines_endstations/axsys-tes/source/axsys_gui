from nbs_gui.models import BaseModel, PVPositionerModel 
from nbs_gui.widgets.manipulator_monitor import RealManipulatorControl, RealManipulatorMonitor

class AxisTuplePVPos(BaseModel):
    default_controller = RealManipulatorControl
    default_monitor = RealManipulatorMonitor

    def __init__(self, name, obj, group, label, **kwargs):
        super().__init__(name, obj, group, label, **kwargs)
        self.real_axes_models = [
            PVPositionerModel(
                name=axis_name, obj=getattr(obj, axis_name), group=group, long_name=axis_name
            )
            for axis_name in obj.component_names
        ]
