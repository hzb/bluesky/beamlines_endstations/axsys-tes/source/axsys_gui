from nbs_gui.models import BaseModel, PVModelRO
from axsys_gui.widgets.signalTuple import SignalTupleMonitor

class SignalTuple(BaseModel):
    default_monitor = SignalTupleMonitor

    def __init__(self, name, obj, group, label, **kwargs):
        super().__init__(name, obj, group, label, **kwargs)
        self.signals = [PVModelRO(name=cpt_name, obj=getattr(obj, cpt_name), group=group, long_name=cpt_name) for cpt_name in obj.read_attrs]

