from nbs_gui.models import BaseModel, PVModel, MotorModel, PVPositionerModel
from ..widgets.energy import UE52EnergyControl, UE52EnergyMonitor
from nbs_core.autoload import instantiateOphyd


class UE52SGMModel:

    def __init__(self, name, obj, group, label, **kwargs):        
        self.name = name
        self.obj = obj
        self.group = group
        self.label = label
        for key, value in kwargs.items():
            setattr(self, key, value)

        self.energy = PVPositionerModel(obj.en.name, obj.en, group=group, long_name=f"{name} Energy")
        self.cff = PVModel(obj.cff.name, obj.cff, group=group, long_name=f"{name} CFF")
        self.harmonic = PVModel(obj.harmonic.name, obj.harmonic, group=group,long_name=f"{name} Harmonic")
        self.slit = PVPositionerModel(obj.slitwidth.name, obj.slitwidth, group=group, long_name=f"{name} Slit Width")
        self.idon = PVModel(obj.ID_on.name, obj.ID_on, group=group, long_name=f"{name} ID on")

class UE52UndulatorModel:

    def __init__(self, name, obj, group, label, **kwargs):
        self.name = name
        self.obj = obj
        self.group = group
        self.label = label
        for key, value in kwargs.items():
            setattr(self, key, value)

        self.gap = PVPositionerModel(obj.gap.name, obj.gap, group, long_name=f"{name} Gap")
        self.shift = PVPositionerModel(obj.shift.name, obj.shift, group, long_name=f"{name} Shift")
        self.mode = PVModel(obj.mode.name, obj.mode, group, long_name=f"{name} Mode")

class UE52EnergyModel:
    default_controller = UE52EnergyControl
    default_monitor = UE52EnergyMonitor

    def __init__(self, name, obj, group, label, **kwargs):
        self.name = "Energy"
        
        self.group = group
        self.label = label

        self.monochromator = UE52SGMModel(name, obj, group, label)

        # find a way to load the undulator in a better way?
        ue52_obj = instantiateOphyd('ue52', {'_target': 'bessyii_devices.undulator.UndulatorUE52', '_group': 'energy', 'prefix': 'UE52ID5R:', 'name': 'ue52'}, cls=None, namespace=None)
        self.undulator = UE52UndulatorModel("ue52", ue52_obj, group, label)  
        
        for key, value in kwargs.items():
            setattr(self, key, value)
        
