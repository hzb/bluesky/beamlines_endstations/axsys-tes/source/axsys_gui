from bluesky_queueserver_api import BPlan
from nbs_gui.plans.base import PlanWidget

class TESCalibrateWidget(PlanWidget):
    def __init__(self, model, parent=None):
        super().__init__(
            model,
            parent,
            time=float,
            energy=float,
            eslit=("Exit Slit", float),
            dwell=float,
            comment=str,
        )
        self.display_name = "TES Calibrate"

    def check_plan_ready(self):
        """
        Check if all selections have been made and emit the plan_ready signal if they have.
        """
        print("Checking XAS Plan")
        print("XAS Ready to Submit")
        self.plan_ready.emit(True)

    def submit_plan(self):
        plan = "tes_calibrate"
        params = self.get_params()
        time = params.pop('time')
        item = BPlan(plan, time, **params)
        self.run_engine_client.queue_item_add(item=item)