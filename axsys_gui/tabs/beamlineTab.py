
from qtpy.QtWidgets import QHBoxLayout, QWidget, QVBoxLayout
from bluesky_widgets.qt.run_engine_client import (
    QtReExecutionControls,
    QtReQueueControls,
    QtReStatusMonitor,
    QtReRunningPlan,
)
from nbs_gui.widgets.utils import HLine
from nbs_gui.widgets.views import AutoControl, AutoControlBox, AutoMonitor, AutoMonitorBox
from nbs_gui.widgets.manipulator_monitor import ManipulatorMonitor

class BeamlineTabWidget(QWidget):
    name = "Beamline Control Tab"

    def __init__(self, model, *args, **kwargs):
        super().__init__(*args, **kwargs)
        run_engine = model.run_engine
        user_status = model.user_status
        beamline = model.beamline
        vbox = QVBoxLayout()
        vbox.addWidget(ManipulatorMonitor(beamline.motors['au1'], model, "v"))
        vbox.addWidget(AutoMonitor(beamline.misc['ph'], model, orientation='v'))
        vbox.addWidget(AutoControl(beamline.primary_energy, model))
        vbox.addWidget(AutoMonitor(beamline.status['bessy2'], model))
        vbox.addWidget(AutoMonitorBox(beamline.detectors, "Detectors", model))
        # vbox.addWidged(AutoMonitor(beamline.mirrors['m1'], model))
        self.setLayout(vbox)
